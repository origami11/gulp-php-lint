var PluginError = require('gulp-util').PluginError;
var path = require('path');
var lint = require('php-lint');
var through = require('through2');    // npm install --save through2

module.exports = function(opts) {

    lint.init(opts);

    var stream = through.obj(function(file, encoding, cb) {
        const filePath = path.relative(process.cwd(), file.path);
        if (file.isNull()) {
            cb(null, file);
            return;
        }

        //        console.log(file.contents.toString());

        if (file.isStream()) {
            // file.contents is a Stream - https://nodejs.org/api/stream.html
            this.emit('error', new PluginError(PLUGIN_NAME, 'Streams not supported!'));

            // or, if you can handle Streams:
            //file.contents = file.contents.pipe(...
            //return cb(null, file);
        } else if (file.isBuffer()) {
            // file.contents is a Buffer - https://nodejs.org/api/buffer.html
            // this.emit('error', new PluginError(PLUGIN_NAME, 'Buffers not supported!'));

            // or, if you can handle Buffers:
            lint.loadFile(file.path);
            //file.contents = ...
            return cb(null, file);
        }
    });

    stream.on('end', function () {
        var messages = lint.getMessages();
        messages.forEach(m => {
            console.log(lint.formatMessage(m));
        });
        console.log('PHP Lint errors:', messages.length);
        lint.reset();
    });

    return stream;
};
